<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Review extends REST_Controller {


	public function __construct(){
		parent::__construct();
	}

    public function index_get(){
		$id_produk = $this->get('id_produk');
   
        $this->db->select('id_review,tbl_konsumen.id_konsumen, nama_konsumen, profile_url, isi_review, tgl_review');
        $this->db->from('tbl_konsumen');
        $this->db->join('tbl_reviews', 'tbl_konsumen.id_konsumen = tbl_reviews.id_konsumen');
        $this->db->where('tbl_reviews.id_produk', $id_produk);
    	$this->db->where('status_review', 'show');	
        $review = $this->db->get()->result();
        $this->response($review, 200);
	}
	
	
    public function index_post(){
        $date = date('Y-m-d');
        $id_konsumen = $this->post('id_konsumen');
        $id_produk = $this->post('id_produk');
        $data = array(
          'id_produk' => $id_produk,
          'id_konsumen' => $id_konsumen,
          'isi_review' => $this->post('isi_review'),
          'tgl_review' => $date,
          'status_review' => 'submit'
           );
        $this->db->where('id_konsumen', $id_konsumen);
        $this->db->where('id_produk', $id_produk);
        $query = $this->db->get('tbl_reviews')->result(); 
        if ($query != null){
              $this->response($query, 409);
            }else{
             $insert = $this->db->insert('tbl_reviews', $data);
                if ($insert) {
                  $this->response(array('status' => 'ok', 200));
                } else {
                  $this->response(array('status' => 'fail', 502));
                }  
            }
        
  }

     public function index_put() {
          $date = date('Y-m-d');
          $id = $this->put('id_review');
              $data = array(
              'isi_review' => $this->put('isi_review'),
              'tgl_review' => $date
              );
             $this->db->where('id_review', $id);
             $update = $this->db->update('tbl_reviews', $data);
             if ($update) {
                $this->response(array('status' => 'ok', 200));
             } else {
                 $this->response(array('status' => 'fail', 502));
             }
         }
}
