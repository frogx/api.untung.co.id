<?php 

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Service_toko extends REST_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->model('M_kategori');
	}

    public function index_get(){
		$id_service = $this->get('id_service');
		$id_detail_service = $this->get('id_detail_service');
        if ($id_service!=null){
            $this->db->select('tbl_detail_service.id_detail_service, id_pesanan_service, total_bayar_service,status_service, file_konfirmasi');
                $this->db->from('tbl_detail_service');
                $this->db->join('tbl_pesanan_service', 'tbl_detail_service.id_detail_service = tbl_pesanan_service.id_detail_service');
                $this->db->where('id_service', $id_service);
                $this->db->where('tbl_pesanan_service.status_service', 'Paid');
                $data = $this->db->get()->result();
                if($data == null){
                    $this->db->select('tbl_detail_service.id_detail_service, id_pesanan_service, total_bayar_service, tbl_pesanan_service.status_service,tbl_pesanan_service.pembayaran, file_konfirmasi');
                    $this->db->from('tbl_detail_service');
                    $this->db->join('tbl_pesanan_service', 'tbl_detail_service.id_detail_service = tbl_pesanan_service.id_detail_service');
                    $this->db->where('id_service', $id_service);
                    $hasil = $this->db->get()->result();
                    if($hasil == null){
                        $this->db->select('id_detail_service, id_service, tbl_detail_service.id_toko, deksripsi_paket, harga_service, nama_toko, foto_toko, alamat, whatsapp');
                        $this->db->from('tbl_detail_service');
                        $this->db->join('tbl_toko', 'tbl_toko.id_toko = tbl_detail_service.id_toko');
                    	$this->db->where('id_service', $id_service);
                    	$this->db->where('flag_submit', '1');	
                        $ekek = $this->db->get()->result();
                        $this->response($ekek, 200);
                    }else {
                        $this->response($hasil, 200);
                    }
                } else {
          $this->response($data, 200);
        }
        } else if ($id_detail_service!=null){
            $this->db->select('id_detail_service, id_service, tbl_detail_service.id_toko, deksripsi_paket, harga_service, nama_toko, foto_toko, alamat, whatsapp');
            $this->db->from('tbl_detail_service');
            $this->db->join('tbl_toko', 'tbl_toko.id_toko = tbl_detail_service.id_toko');
        	$this->db->where('id_detail_service', $id_detail_service);
            $ekek = $this->db->get()->result();
            $this->response($ekek, 200);
        }
        else {
          $this->response(array('status' => 'fail', 502));
        }
	}
  
}