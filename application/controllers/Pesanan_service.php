<?php 

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Pesanan_service extends REST_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->model('M_kategori');
	}

    public function index_get(){
		$id = $this->get('id_detail_service');
		$id_konsumen = $this->get('id_konsumen');
		$telek = $this->get('kode');


        if ($id!=null) {
        	$this->db->select('id_detail_service, id_service, tbl_detail_service.id_toko, deksripsi_paket, harga_service, nama_toko, foto_toko, alamat, no_toko_service, no_hp_toko');
            $this->db->from('tbl_detail_service');
            $this->db->join('tbl_toko', 'tbl_toko.id_toko = tbl_detail_service.id_toko');
        	$this->db->where('id_detail_service', $id);
            $ekek = $this->db->get()->result();
            $this->response($ekek, 200);
        }

        else if($id_konsumen!=null) {
            $this->db->where('id_konsumen', $id_konsumen);	
        	$this->db->where('status_service', 'aktif');	
            $data = $this->db->get('tbl_service')->result();
            $this->response($data, 200);
        }
        else if($telek!=null){
            $total_belanja = 10000;
            $kode_unix = rand(10,1000);
            $harga_fix = substr($total_belanja, 0, -3) . $kode_unix;
            $this->response(array('harga' => $harga_fix), 200);
        }
        else {
          $this->response(array('status' => 'fail', 502));
        }
	}

  public function index_post()
  {
   
    $data = array(
      'id_detail_service' => $this->post('id_detail_service'),
      'total_bayar_service' => $this->post('uang_muka'),
      'pembayaran' => $this->post('pembayaran'),
      'status_service' => 'Unpaid'
       );
    $insert = $this->db->insert('tbl_pesanan_service', $data);
    if ($insert) {
      $id = $this->db->insert_id();
            $this->db->where('id_pesanan_service', $id);
            $hasil = $this->db->get('tbl_pesanan_service')->result();
            $this->response($hasil, 200);
    } else {
      $this->response(array('status' => 'fail', 502));
    }
  }
  
  function index_put() {
    $id = $this->put('id');
    $data = array(
      'status_service' => 'Paid',
      'file_konfirmasi' => $this->put('foto')
       );
    $this->db->where('id_pesanan_service', $id);
    $update = $this->db->update('tbl_pesanan_service', $data);
     if ($update) {
         $this->response($data, 200);
     } else {
         $this->response(array('status' => 'fail', 502));
     }
     }
  
  
}
