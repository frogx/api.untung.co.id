<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Produk_filter extends REST_Controller {

	public function __construct(){
		parent::__construct();
		// $this->load->model('M_produk');
	}

	public function index_get(){
	    $page = $this->get('page');
	    $number = 6;
	    $sort = $this->get('sort');
	    $max = $this->get('max');
	    $min = $this->get('min');
	    $star = $this->get('star');
	    $offset = $number * ($page - 1);
		$id_kategori = $this->get('id_kategori');

		$this->db->select('tbl_produk.*, nama_toko, no_hp_toko, kota_toko');
		$this->db->from('tbl_produk');
		$this->db->join('tbl_toko', 'tbl_toko.id_toko = tbl_produk.id_toko');
		
    	if($sort!=null) {
    	   $this->db->where('id_kategori', $id_kategori);
    	   $this->db->where('status_produk', 'verifikasi');
           $this->db->order_by("harga_produk", $sort);
		   $this->db->limit($number, $offset);
		   $produk = $this->db->get()->result();
    	   $this->response($produk, 200);
    	} 
    	else if($max!=null && $min!=null && $star!=null) {
    	       $this->db->where('id_kategori', $id_kategori);
        	   $this->db->where('status_produk', 'verifikasi');
        	   $this->db->where('(jumlah_rating/jumlah_ulasan) >=', $star);
        	   $this->db->where('harga_produk >=', $min);
        	   $this->db->where('harga_produk <=', $max);
               $this->db->order_by('harga_produk', 'ASC');
			   $this->db->limit($number, $offset);
			   $produk = $this->db->get()->result();
        	   $this->response($produk, 200);
    	} 
    	else if($star!=null && $max!=null) {
    	       $this->db->where('id_kategori', $id_kategori);
        	   $this->db->where('status_produk', 'verifikasi');
        	   $this->db->where('(jumlah_rating/jumlah_ulasan) >=', $star);
        	   $this->db->where('harga_produk <=', $max);
			   $this->db->order_by('harga_produk', 'ASC');
			   $this->db->limit($number, $offset);
			   $produk = $this->db->get()->result();
        	   $this->response($produk, 200);
    	    
    	}
    	else if($star!=null && $min!=null) {
    	       $this->db->where('id_kategori', $id_kategori);
        	   $this->db->where('status_produk', 'verifikasi');
        	   $this->db->where('(jumlah_rating/jumlah_ulasan) >=', $star);
        	   $this->db->where('harga_produk >=', $min);
			   $this->db->order_by('harga_produk', 'ASC');
			   $this->db->limit($number, $offset);
			   $produk = $this->db->get()->result();
        	   $this->response($produk, 200);
    	    
    	}
    	else if($max!=null && $min!=null) {
    	       $this->db->where('id_kategori', $id_kategori);
        	   $this->db->where('status_produk', 'verifikasi');
        	   $this->db->where('harga_produk >=', $min);
        	   $this->db->where('harga_produk <=', $max);
			   $this->db->order_by('harga_produk', 'ASC');
			   $this->db->limit($number, $offset);
			   $produk = $this->db->get()->result();
        	   $this->response($produk, 200);
    	    
    	}
    	else if($max!=null) {
    	       $this->db->where('id_kategori', $id_kategori);
        	   $this->db->where('status_produk', 'verifikasi');
        	   $this->db->where('harga_produk <=', $max);
			   $this->db->order_by('harga_produk', 'ASC');
			   $this->db->limit($number, $offset);
			   $produk = $this->db->get()->result();
        	   $this->response($produk, 200);
    	}
    	else if($min!=null) {
    	       $this->db->where('id_kategori', $id_kategori);
        	   $this->db->where('status_produk', 'verifikasi');
        	   $this->db->where('harga_produk >=', $min);
			   $this->db->order_by('harga_produk', 'ASC');
			   $this->db->limit($number, $offset);
			   $produk = $this->db->get()->result();
        	   $this->response($produk, 200);
    	}
    		else if($star!=null) {
    	       $this->db->where('id_kategori', $id_kategori);
        	   $this->db->where('status_produk', 'verifikasi');
        	   $this->db->where('(jumlah_rating/jumlah_ulasan) >=', $star);
			   $this->db->order_by('harga_produk', 'ASC');
			   $this->db->limit($number, $offset);
			   $produk = $this->db->get()->result();
        	   $this->response($produk, 200);
    	} 
    	else {
        	$this->response(array('status' => 'fail', 'keterangan' => 'tidak ada artribut yang dimasukan'), 502);
        }
	}



}
