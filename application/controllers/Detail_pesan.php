<?php 

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Detail_pesan extends REST_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->model('M_kategori');
	}

	public function index_get(){
    $id = $this->get('id');
    $this->db->select('tbl_alamat_pengiriman_konsumen. *, tbl_detail_pesan.*, tbl_produk.id_toko, tbl_produk.nama_produk, tbl_produk.gambar_1, nama_toko, foto_toko, kota_toko, alamat, tbl_grup_detail_pesanan.estimasi_pengiriman, tbl_grup_detail_pesanan.resi_pengiriman, tbl_grup_detail_pesanan.status_barang_pesanan');
    $this->db->where('tbl_detail_pesan.id_pesanan', $id);
    $this->db->from('tbl_detail_pesan');
    $this->db->join('tbl_produk', 'tbl_produk.id_produk = tbl_detail_pesan.id_produk');
    $this->db->join('tbl_toko', 'tbl_toko.id_toko = tbl_produk.id_toko');
    $this->db->join('tbl_alamat_pengiriman_konsumen', 'tbl_alamat_pengiriman_konsumen.id_alamat_pengiriman = tbl_detail_pesan.id_alamat_pengiriman');
    $this->db->join('tbl_grup_detail_pesanan', 'tbl_grup_detail_pesanan.grup_detail_pesanan_id = tbl_detail_pesan.grup_detail_pesanan AND tbl_grup_detail_pesanan.id_pesanan = tbl_detail_pesan.id_pesanan');
    $query = $this->db->get()->result();
    $this->response($query, 200);
  }

  public function index_post()
  {
    $date = date('Y-m-d');
    $id = $this->post('id_pesanan');
    $data = array(
      'id_pesanan' 		=> $id,
      'id_alamat_pengiriman' 		=> $this->post('id_alamat_pengiriman'),
      'id_produk' 	=> $this->post('id_produk'),
      'jumlah_pesan'		=> $this->post('jumlah_pesan'),
      'sub_total'		=> $this->post('sub_total'),
      'layanan_pengiriman'		=> $this->post('kurir'),
      'ongkos_kirim'		=> $this->post('total_pengiriman'),
      'ongkir_per_item'		=> $this->post('total_pengiriman'),
      'jumlah_total_pesanan'		=> $this->post('jumlah_total_pesanan'),
      'catatan_produk'		=> $this->post('catatan'),
      'grup_detail_pesanan'		=> $this->post('grub_detail_pesan'),
       );
    $insert = $this->db->insert('tbl_detail_pesan', $data);
    if ($insert) {
    //   $this->db->select('tbl_alamat_pengiriman_konsumen. *, tbl_detail_pesan.*, tbl_produk.id_toko, tbl_produk.nama_produk, tbl_produk.gambar_1, nama_toko, foto_toko, kota_toko, alamat, tbl_grup_detail_pesanan.estimasi_pengiriman, tbl_grup_detail_pesanan.resi_pengiriman, tbl_grup_detail_pesanan.status_barang_pesanan');
    //   $this->db->where('tbl_detail_pesan.id_pesanan', $id);
    //   $this->db->from('tbl_detail_pesan');
    //   $this->db->join('tbl_produk', 'tbl_produk.id_produk = tbl_detail_pesan.id_produk');
    //   $this->db->join('tbl_toko', 'tbl_toko.id_toko = tbl_produk.id_toko');
    //   $this->db->join('tbl_alamat_pengiriman_konsumen', 'tbl_alamat_pengiriman_konsumen.id_alamat_pengiriman = tbl_detail_pesan.id_alamat_pengiriman');
    //   $this->db->join('tbl_grup_detail_pesanan', 'tbl_grup_detail_pesanan.grup_detail_pesanan_id = tbl_detail_pesan.grup_detail_pesanan AND tbl_grup_detail_pesanan.id_pesanan = tbl_detail_pesan.id_pesanan');
    //   $query = $this->db->get()->result();
      $query = array('status'=>'sippp');
      $this->response($query, 200);
    } else {
      $this->response(array('status' => 'fail', 502));
    }
  }
}
