<?php 

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Alamat extends REST_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->model('M_kategori');
	}

	public function index_get(){
        $id = $this->get('id_konsumen');
        $this->db->where('id_konsumen', $id);
        $query = $this->db->get('tbl_alamat_pengiriman_konsumen')->result();
        $this->response($query, 200);
	}
	
	public function index_post(){
        $id = $this->post('id_konsumen');
        $data = array(
          'id_konsumen' => $id,
          'nama_alamat_pengiriman' => $this->post('nama_alamat_pengiriman'),
          'nama_penerima_pengiriman' => $this->post('nama_penerima_pengiriman'),
          'alamat_konsumen_penerima' => $this->post('alamat_konsumen_penerima'),
          'provinsi_konsumen_penerima' => $this->post('provinsi_konsumen_penerima'),
          'id_provinsi_konsumen_penerima' => $this->post('id_provinsi_konsumen_penerima'),
          'kota_konsumen_penerima' => $this->post('kota_konsumen_penerima'),
          'id_kota_konsumen_penerima' => $this->post('id_kota_konsumen_penerima'),
          'id_kecamatan_pengiriman' => $this->post('id_kecamatan_pengiriman'),
          'kecamatan_pengiriman' => $this->post('kecamatan_pengiriman'),
          'kode_pos_pengiriman' => $this->post('kode_pos_pengiriman'),
          'no_hp_konsumen_penerima' => $this->post('no_hp_konsumen_penerima')
           );
            $insert = $this->db->insert('tbl_alamat_pengiriman_konsumen', $data);
            if ($insert) {
                $this->db->where('id_konsumen', $id);
                $query = $this->db->get('tbl_alamat_pengiriman_konsumen')->result();
                $this->response($query, 200);
            } else {
                $this->response($data, 502);
            }  
             
        }

    public function index_delete($id){
        $data = array(
            'id_alamat_pengiriman' => $id
        );
        $delete = $this->db->delete('tbl_alamat_pengiriman_konsumen', $data);
        if($delete){
            $this->response(array('status' => 'sukses', 'keterangan' => 'Berhasil Menghapus Data'), 200);
        } else {
            $this->response(array('status' => 'fail', 'keterangan' => 'Gagal Menghapus Data'), 502);
        }
    }

    public function index_put() {
        $id = $this->put('id');
        $id_konsumen = $this->post('id_konsumen');
        $data = array(
            'id_konsumen' => $id_konsumen,
            'nama_alamat_pengiriman' => $this->post('nama_alamat_pengiriman'),
            'nama_penerima_pengiriman' => $this->post('nama_penerima_pengiriman'),
            'alamat_konsumen_penerima' => $this->post('alamat_konsumen_penerima'),
            'provinsi_konsumen_penerima' => $this->post('provinsi_konsumen_penerima'),
            'id_provinsi_konsumen_penerima' => $this->post('id_provinsi_konsumen_penerima'),
            'kota_konsumen_penerima' => $this->post('kota_konsumen_penerima'),
            'id_kota_konsumen_penerima' => $this->post('id_kota_konsumen_penerima'),
            'id_kecamatan_pengiriman' => $this->post('id_kecamatan_pengiriman'),
            'kecamatan_pengiriman' => $this->post('kecamatan_pengiriman'),
            'kode_pos_pengiriman' => $this->post('kode_pos_pengiriman'),
            'no_hp_konsumen_penerima' => $this->post('no_hp_konsumen_penerima')
                );
          $this->db->where('id_alamat_pengiriman', $id);
          $update = $this->db->update('tbl_alamat_pengiriman_konsumen', $data);
          if ($update) {
             $this->db->where('id_konsumen', $id_konsumen);
             $alamat = $this->db->get('tbl_alamat_pengiriman_konsumen')->result(); 
             $this->response($alamat, 200);
          } else {
              $this->response($data, 502);
          }
      }

}
