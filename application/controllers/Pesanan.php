<?php 

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Pesanan extends REST_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->model('M_kategori');
	}

	public function index_get(){
	$id_konsumen = $this->get('id_konsumen');
	if($id_konsumen!=null){
	    $this->db->where('id_konsumen', $id_konsumen);
        $this->db->from('tbl_pembayaran_pesanan');
        $this->db->join('tbl_rekening_pembayaran', 'tbl_rekening_pembayaran.id_rekening_pembayaran = tbl_pembayaran_pesanan.id_rekening_pembayaran');
        $query = $this->db->get()->result();
        $this->response($query, 200);
	}
    else{
        $this->response(array('status' => 'eror'), 502);
    }
    
  }

  public function index_post()
  {
    $date = date('Y-m-d');
    $date_tempo = date('Y-m-d', strtotime($date. ' + 2 days'));
    $data = array(
      'id_konsumen' => $this->post('id_konsumen'),
      'jumlah_pembayaran' => $this->post('total_pesanan'),
      'jumlah_ongkir_pembayaran' => $this->post('total_pengiriman'),
      'id_rekening_pembayaran' => $this->post('id_rekening_pembayaran'),
      'flag_kode_diskon' => $this->post('flag_kode_diskon'),
      'id_diskon' => $this->post('id_diskon'),
      'jumlah_potongan' => $this->post('jumlah_potongan'),
      'tanggal_order'         => $date,
      'tanggal_tempo_pembayaran' => $date_tempo,
      'nama_pemilik_rekening' => $this->post('nama_pemilik_rekening'),
      'no_rekening_pembayaran_pesanan' => $this->post('no_rekening_pembayaran_pesanan'),
      'status_pembayaran'    => 'menunggu',
      'kode_unix'         => $this->post('kode_unix')
       );
    $insert = $this->db->insert('tbl_pembayaran_pesanan', $data);
    if ($insert) {
      $id = $this->db->insert_id();
      $this->db->where('id_pembayaran_pesanan', $id);
      $query = $this->db->get('tbl_pembayaran_pesanan')->result();
      $this->response($query, 200);
    } else {
      $this->response(array('status' => 'fail', 502));
    }
  }

  
  function index_put() {
       $id = $this->put('id_pembayaran_pesanan');
          $data = array(
          'file_konfirmasi_pembayaran' => $this->put('foto'),
          'status_pembayaran'    => 'konfirmasi'
           );
         $this->db->where('id_pembayaran_pesanan', $id);
         $update = $this->db->update('tbl_pembayaran_pesanan', $data);
         if ($update) {
              $this->response(array('status' => 'sukses', 200));
         } else {
             $this->response(array('status' => 'fail', 502));
         }
     }
  
  
}
